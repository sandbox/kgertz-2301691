/**
 * @file 
 * Defines a utility object for data transmission and defines a Drupal behavior 
 * that attaches a control to canvas elements for sending image data to the
 * browser.
 */


// first create a utility object, then attach a drupal behavior

Drupal.canvascapture = {

  /**
   * 
   * 
   * @param {type} canvas
   * @param {type} options
   *        Options can be passed as json or be included as data- arguments in 
   *        the canvas element:
   *        
   *        --data-attach-to="entity_type entity–id field_name mode"
   *        if this option is set, the file will not only be created on the 
   *        server but also be attached to a specific field in the given entity. 
   *        Clearly, this field must be a file field. You can use this e.g. to
   *        attach a created image to a nodes image field.
   *        
   *        
   *        --data-file-path="path/without/filename"
   *        the path ehere to store the image file
   *        
   *        --data-file-name="filename_without_extension" 
   *        The filename without extension - the module will ONLY produce
   *        png images
   *        
   *        If options are passed as json directly to this function, they will 
   *        override any canvas attributes set in the document. 
   *    
   * @returns {undefined}
   */
  capture: function(canvas, options) {
    var $ = jQuery;
    $(canvas).removeClass('canvascapture-ready').addClass('canvascapture-sending');
    
    var data = canvas.toDataURL();
    var opts = Drupal.canvascapture.parseOptions(canvas);
    // send the canvas data back to the server so that it can store an image
    jQuery.ajax({
      type: 'POST',
      url: '/canvascapture/convert',
      data: {
        canvas_id: canvas.id,
        options: opts,
        data: data,
      },
      success: function(data, textStatus, jqXHR) {
        var response = $.parseJSON(data);
        if (console) {
          console.log('canvas data returned from server:');
          console.log($.parseJSON(data));
        }
        var canvas = $('#' + response.canvas_id);
        var controlLink = $(canvas).next();
        $(canvas).removeClass('canvascapture-sending').addClass('canvascapture-sent');
        $(controlLink).attr('title', Drupal.t('This canvas has been stored as an image'));
      },
      dataType: 'application/base64'
    });
  },
  
  parseOptions: function(canvas, passedOptions) {
    var $ = jQuery;
    var options = {};
    
    var path = $(canvas).attr('data-file-path');
    if (path) {
      options.path = path;
    } else if (passedOptions && passedOptions.path) {
      options.path = passedOptions.path;
    }
    
    var name = $(canvas).attr('data-file-name');
    if (name) {
      options.name = name;
    } else if (passedOptions && passedOptions) {
      options.name = passedOptions.name;
    }
    // --data-attach-to="entity_type entity–id field_name mode"
    var attach = $(canvas).attr('data-attach-to');
    if (attach) {
      var attachTokens = attach.split(' ');
      if (! attachTokens.length == 4 ) {
        if (console) console.log("Wrong number of arguments in attribute data-attach-to, canvas id: " + canvas.id);
      } else {
        options.attach_entity_type = attachTokens[0];
        options.attach_entity_id = attachTokens[1];
        options.attach_field = attachTokens[2];
        options.attach_mode = attachTokens[3];
      }
    } else if (passedOptions && passedOptions.attach) {
      options.attach = passedOptions.attach;
    }
    // data-file-path="pictures" data-file-name="foo" data-attach-to="file 866 field_brochure_thumbnail append" 
    return options;
  }
}


Drupal.behaviors.canvasCaptureUI = {
  attach: function(context, settings) {
    var $ = jQuery;
    var canvasList = $('canvas').not('canvascapture-processed')
            .addClass('canvascapture-processed')
            .addClass('canvascapture-ready');

    for (var i = 0; i < canvasList.length; i++) {
      // create a link for the control as a jquery object and
      // attach a click handler immediately
      var controlLink = $('<a href="#canvascapture-control-' + i
              + '"id="canvascapture-control-' + i + '" title="'
              + Drupal.t("Capture this canvas and store it on the server")
              + '"class="canvascapture-control"></a>');

      $(controlLink).bind('click', function() {
        // as this function should be the only click handler,
        // prevent browser default behaviour and stop bubbling of the event
        event.preventDefault();
        event.stopImmediatePropagation();
        // as the control has been attached right after the canvas,
        // fetch the previous sibling which is tha canvas
        var canvas = $(this).prev('canvas')[0];
        Drupal.canvascapture.capture(canvas);
      });
      // after the control element is set up, attach it to the document
      // right after the canvas
      $(canvasList[i]).after(controlLink);
    }
  }
};  