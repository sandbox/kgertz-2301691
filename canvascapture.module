<?php

/**
 * @file
 * Provides a tool for capturing canvas data as an image.
 */

/**
 * Implements hook_menu().
 */
function canvascapture_menu() {
  $items = array();
  $items['canvascapture/convert'] = array(
    'page callback' => 'canvascapture_convert',
    'file' => 'includes/canvascapture.convert.inc',
    'access arguments' => array('create image from canvas'),
    'type' => MENU_CALLBACK,
  );
  $items['admin/config/media/canvascapture'] = array(
    'title' => 'Canvas Capture',
    'description' => 'Administer settings for canvas capture',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('canvascapture_admin'),
    'file' => 'includes/canvascapture.admin.inc',
    'access arguments' => array('administer canvascapture settings'),
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function canvascapture_permission() {
  return array(
    'create image from canvas' => array(
      'title' => t('Create image from canvas'),
      'description' => t('Create image from canvas and store it on server.'),
    ),
    'administer canvascapture settings' => array(
      'title' => t('Administer settings for canvas capture'),
      'description' => t('Administer paths for file creation etc.'),
    ),
  );
}

/**
 * Implements hook_page_build().
 */
function canvascapture_page_build(&$page) {
  // add the relevant js + css only if the current user has the right
  // to 'create image from canvas'
  if (user_access('create image from canvas')) {
    $path = drupal_get_path('module', 'canvascapture');
    drupal_add_js($path . '/js/canvascapture.js');
    drupal_add_css($path . '/css/canvascapture.css');
  }
}
