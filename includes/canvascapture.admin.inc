<?php

/**
 * @file
 * Defines the adminsitrative interface for the canvascapture module
 */

/**
 * Implements hook_admin().
 */
function canvascapture_admin() {
  $form = array();

  $default_fs = variable_get('canvascapture_file_system', 'public://');
  $form['canvascapture_file_system'] = array(
    '#title' => t('File system settings'),
    '#description' => t('Specify whether the created files should be in private or in public file system'),
    '#type' => 'radios',
    '#options' => drupal_map_assoc(array('public://', 'private://')),
    '#default_value' => $default_fs,
  );
  $default_path = variable_get('canvascapture_default_path', 'pictures/canvas-captures');
  $form['canvascapture_default_path'] = array(
    '#title' => t('Save path'),
    '#type' => 'textfield',
    '#default_value' => $default_path,
    '#description' => t('The default path for saving converted canvas images'),
  );

  return system_settings_form($form);
}
