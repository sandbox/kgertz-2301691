<?php

/**
 * @file
 * Provides functions to convert a base64 encoded image representation to an image file.
 */

/**
 * Callback function for ajax, takes base64 encoded image data received in
 * $_POST and stores it as a file
 *
 * @global type $language
 */
function canvascapture_convert() {

  global $language;
  $options = $_POST;
  $default_fs = variable_get('canvascapture_file_system', 'public://');
  $default_path = variable_get('canvascapture_default_path', 'pictures/canvas-captures');

  $data = check_plain($_POST['data']);
  $path = check_plain($options['options']['path']);
  $name = check_plain($options['options']['name']);
  $canvas_id = check_plain($options['canvas_id']);

  // construct the path for saving the image
  $fullpath = DRUPAL_ROOT . '/' ;
  $cc_fs = variable_get('canvascapture_file_system', 'public://');
  $public = variable_get('file_public_path', conf_path() . '/files');
  $private = variable_get('file_private_path', 'private/files');

  switch ($cc_fs) {
    case 'public://' : $fullpath .= $public; break;
    case 'private://' : $fullpath .= $private; break;
  }
  if ($path) {
    $fullpath .= '/' . $path;
  }
  else {
    $fullpath .= '/' . variable_get('canvascapture_default_path', 'pictures/canvas-captures');
  }

  $filename = ($name) ? $name : $canvas_id;
  $filename .= '.png';
  // use transliteration, if available
  if (function_exists('transliteration_clean_filename')) {
    $filename = transliteration_clean_filename($filename, '_', $language->language);
  }

  file_prepare_directory($fullpath, FILE_CREATE_DIRECTORY);
  $trimmed_data = str_replace('data:image/png;base64,', '', $data);
  $data = base64_decode($trimmed_data, TRUE);
  // we could now write the decoded base64 data right into a file, but...
  // we save about 50% of the filesize if we create an image representation
  // and save it via tyhe GD library.
  $img = imagecreatefromstring($data);
  imagepng($img, $fullpath . '/' . $filename);

  $response = array(
    'status' => 'OK',
    'message' => t('File saved to') . ' ' . $fullpath . '/' . $filename,
    // send back the canvas id so that the javascript can modify the
    // canvas classes in order to mark the control
    'canvas_id' => $canvas_id,
  );

  drupal_json_output($response);
  exit();
}

